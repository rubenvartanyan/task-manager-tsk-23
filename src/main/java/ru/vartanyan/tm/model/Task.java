package ru.vartanyan.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.entity.IWBS;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractBusinessEntity implements IWBS {

    private String projectId;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final @NotNull String projectId) {
        this.projectId = projectId;
    }

}
