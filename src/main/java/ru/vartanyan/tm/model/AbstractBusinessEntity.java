package ru.vartanyan.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.vartanyan.tm.enumerated.Status;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
public class AbstractBusinessEntity extends AbstractEntity{

    public String name = "";

    public String description = "";

    public String userId;

    public Status status = Status.NOT_STARTED;

    public Date dateStarted;

    public Date dateFinish;

    public Date created = new Date();

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public String toString() {
        return getId() + ": " + name + "; " + description + "; " + "User Id: " + getUserId();
    }

}
