package ru.vartanyan.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.AbstractBusinessEntity;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IBusinessRepository<E>, IService<E> {

    @NotNull E add(final @NotNull String name,
                   final @NotNull String description,
                   final @NotNull String userId) throws Exception;

    void updateEntityById(String id,
                          String name,
                          String description,
                          String userId) throws Exception;

    void updateEntityByIndex(final @NotNull Integer index,
                             final @NotNull String name,
                             final @NotNull String description,
                             final @NotNull String userId) throws Exception;

    void startEntityById(final @NotNull String id,
                         final @NotNull String userId) throws Exception;

    void startEntityByName(final @NotNull String name,
                           final @NotNull String userId) throws Exception;

    void startEntityByIndex(final @NotNull Integer index,
                            final @NotNull String userId) throws Exception;

    void finishEntityById(final @NotNull String id,
                          final @NotNull String userId) throws Exception;

    void finishEntityByName(final @NotNull String name,
                            final @NotNull String userId) throws Exception;

    void finishEntityByIndex(final @NotNull Integer index,
                             final @NotNull String userId) throws Exception;

    void updateEntityStatusById(final @NotNull String id,
                                final @NotNull Status status,
                                final @NotNull String userId) throws Exception;

    void updateEntityStatusByName(final @NotNull String name,
                                  final @NotNull Status status,
                                  final @NotNull String userId) throws Exception;

    void updateEntityStatusByIndex(final @NotNull Integer index,
                                   final @NotNull Status status,
                                   final @NotNull String userId) throws Exception;

    void showEntityByName(final @NotNull String name,
                          final @NotNull String userId) throws Exception;

}
