package ru.vartanyan.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable Status getStatus();

    void setStatus(final @NotNull Status status);

}
