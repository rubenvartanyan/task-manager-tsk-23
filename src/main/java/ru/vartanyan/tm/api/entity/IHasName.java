package ru.vartanyan.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IHasName {

    @Nullable String getName();

    void setName(final @NotNull String name);

}
