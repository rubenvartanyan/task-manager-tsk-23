package ru.vartanyan.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.model.AbstractEntity;

public interface IRepository<E extends AbstractEntity> {

    @NotNull E add(final @NotNull E entity);

    void remove(final @NotNull E entity);

    @NotNull E findById(final @NotNull String id) throws Exception;

    void removeById(final @NotNull String id) throws Exception;

}
