package ru.vartanyan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    @Nullable List<Task> findAllByProjectId(final @NotNull String projectId,
                                            final @NotNull String userId);

    void removeAllByProjectId(final @NotNull String projectId,
                              final @NotNull String userId);

    void bindTaskByProjectId(final @NotNull String projectId,
                             final @NotNull String taskId,
                             final @NotNull String userId);

    @Nullable Task unbindTaskFromProject(final @NotNull String projectId,
                               final @NotNull String taskId,
                               final @NotNull String userId);

}
