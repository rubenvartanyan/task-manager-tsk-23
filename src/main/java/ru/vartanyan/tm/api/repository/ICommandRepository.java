package ru.vartanyan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractCommand;
import java.util.Collection;

public interface ICommandRepository {

    @NotNull Collection<AbstractCommand> getCommand();

    @NotNull Collection<AbstractCommand> getArguments();

    @NotNull Collection<String> getCommandNames();

    @NotNull Collection<String> getCommandsArgs();

    @NotNull AbstractCommand getCommandByName(final @NotNull String name);

    @NotNull AbstractCommand getCommandByArg(final @NotNull String arg);

    void add(final @NotNull AbstractCommand command);

}
