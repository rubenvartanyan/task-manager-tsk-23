package ru.vartanyan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public interface IUserRepository extends IRepository<User>{

    @Nullable User findByLogin(@NotNull final String login);

    void removeByLogin(@NotNull final String login);

}
