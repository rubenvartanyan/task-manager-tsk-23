package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void info(final @NotNull String message);

    void debug(final @NotNull String message);

    void command(final @NotNull String message);

    void error(final @NotNull Exception e);

}
