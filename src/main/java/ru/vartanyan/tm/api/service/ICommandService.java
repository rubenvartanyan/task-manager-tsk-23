package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractCommand;
import ru.vartanyan.tm.model.Command;

import java.util.Collection;

public interface ICommandService {

   @Nullable AbstractCommand getCommandByName(final @NotNull String name);

   @Nullable AbstractCommand getCommandByArg(final @NotNull String arg);

   @Nullable Collection<AbstractCommand> getArguments();

   @Nullable Collection<AbstractCommand> getCommands();

   @Nullable Collection<String> getListArgumentName();

   @Nullable Collection<String> getListCommandName();

   void add(final @NotNull AbstractCommand command);

}
