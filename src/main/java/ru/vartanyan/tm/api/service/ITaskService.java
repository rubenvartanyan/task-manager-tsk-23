package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.IBusinessService;
import ru.vartanyan.tm.model.Task;

public interface ITaskService extends IBusinessService<Task> {

    @NotNull Task add(final @NotNull String name,
                      final @NotNull String description,
                      final @NotNull String userId) throws Exception;

}
