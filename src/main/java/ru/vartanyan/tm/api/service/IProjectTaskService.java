package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    // SHOW ALL TASKS FROM PROJECT
    @Nullable List<Task> findAllTaskByProjectId(final @NotNull String projectId,
                                                final @NotNull String userId) throws Exception;

    // ADD TASK TO PROJECT
    void bindTaskByProjectId(final @NotNull String projectId,
                             final @NotNull String taskId,
                             final @NotNull String userId) throws Exception;

    // REMOVE TASK FROM PROJECT
    @NotNull Task unbindTaskFromProject(final @NotNull String projectId,
                               final @NotNull String taskId,
                               final @NotNull String userId) throws Exception;

    // REMOVE ALL TASKS FROM PROJECT AND THEN PROJECT
    void removeProjectById(final @NotNull String projectId,
                           final @NotNull String userId) throws Exception;

}
