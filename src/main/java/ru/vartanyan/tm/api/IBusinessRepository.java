package ru.vartanyan.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    void clear(final @NotNull String userId);

    @Nullable List<E> findAll(final @NotNull String userId);

    @Nullable E findById(final @NotNull String id,
               final @NotNull String userId);

    void removeById(final @NotNull String id,
                    final @NotNull String userId);

    @Nullable E findOneByIndex(@NotNull Integer index,
                     @NotNull String userId) throws Exception;

    @Nullable E findOneByName(final @NotNull String name,
                    final @NotNull String userId) throws Exception;

    void removeOneByIndex(final @NotNull Integer index,
                          final @NotNull String userId) throws Exception;

    void removeOneByName(final @NotNull String name,
                         final @NotNull String userId) throws Exception;

    @Nullable List<E> findAll(final @NotNull Comparator<E> comparator,
                    final @NotNull String userId);

    void showEntity(final @NotNull E entity);

}
