package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.service.IAuthService;
import ru.vartanyan.tm.api.service.IUserService;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.empty.EmptyPasswordException;
import ru.vartanyan.tm.exception.system.AccessDeniedException;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.exception.system.UserLockedException;
import ru.vartanyan.tm.exception.system.WrongRoleException;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;
    private String userId;

    public AuthService(final @NotNull IUserService userService) {
        this.userService = userService;
    }

    @Override
    public @Nullable User getUser() throws Exception {
        return userService.findById(userId);
    }

    @Override
    public @NotNull String getUserId() throws Exception {
        return userId;
    }

    @Override
    public boolean isNotAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(@Nullable final String login,
                      @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        if (user.getLocked()) throw new UserLockedException();
        userId = user.getId();
    }

    @Override
    public void registry(@Nullable final String login,
                         @Nullable final String password,
                         @Nullable final String email) throws Exception {
        userService.create(login, password, email);
    }

    @Override
    public void checkRoles(final @Nullable Role... roles) throws Exception {
        if (roles == null || roles.length == 0) return;
        final User user = getUser();
        if (user == null) throw new NotLoggedInException();
        final Role role = user.getRole();
        if (role == null) throw new WrongRoleException();
        for (final Role item: roles) {
            if (item.equals(role)) return;
        }
        throw new WrongRoleException();
    }

}
