package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.ICommandRepository;
import ru.vartanyan.tm.api.service.ICommandService;
import ru.vartanyan.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final @NotNull ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public @Nullable AbstractCommand getCommandByName(@NotNull String name) {
        return commandRepository.getCommandByName(name);
    }

    @Override
    public @Nullable AbstractCommand getCommandByArg(@NotNull String arg) {
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public @Nullable Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public @Nullable Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommand();
    }

    @Override
    public @Nullable Collection<String> getListArgumentName() {
        return commandRepository.getCommandsArgs();
    }

    @Override
    public @Nullable Collection<String> getListCommandName() {
        return commandRepository.getCommandNames();
    }

    @Override
    public void add(final @Nullable AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}
