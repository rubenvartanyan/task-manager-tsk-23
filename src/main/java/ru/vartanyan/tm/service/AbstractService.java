package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.model.AbstractEntity;

public class AbstractService<E extends AbstractEntity> implements IService<E> {

    private final IRepository<E> repository;

    public AbstractService(final @NotNull IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public @NotNull E add(final @Nullable E entity) {
        if (entity == null) return null;
        return repository.add(entity);
    }

    @Override
    public void remove(final @Nullable E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

    @Override
    public @NotNull E findById(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public void removeById(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.removeById(id);
    }

}
