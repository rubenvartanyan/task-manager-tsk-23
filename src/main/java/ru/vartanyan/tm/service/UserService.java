package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.api.service.IUserService;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.*;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.util.HashUtil;

public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User findByLogin(final @NotNull String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public void removeByLogin(final @NotNull String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @Override
    public @NotNull User create(final @NotNull String login,
                                final @NotNull String password) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USER);
        userRepository.add(user);
        return user;
    }

    @Override
    public void create(final @NotNull String login,
                       final @NotNull String password,
                       final @NotNull String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = create(login, password);
        user.setEmail(email);
    }

    @Override
    public void create(@NotNull String login,
                       @NotNull String password,
                       @NotNull Role role) throws Exception {
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        user.setRole(role);
    }

    @Override
    public void setPassword(final @NotNull String userId,
                            final @NotNull String password) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(userId);
        if (user == null) return;
        final String hash = HashUtil.salt(password);
        user.setPasswordHash(hash);
    }

    @Override
    public void updateUser(final @NotNull String userId,
                           final @NotNull String firstName,
                           final @NotNull String lastName,
                           final @NotNull String middleName
                           ) throws Exception {
        final User user = findById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
    }

    @Override
    public void unlockUserByLogin(@NotNull String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setLocked(false);
    }

    @Override
    public void unlockUserById(@NotNull String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final User user = userRepository.findById(id);
        if (user == null) return;
        user.setLocked(false);
    }

    @Override
    public void lockUserByLogin(@NotNull String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setLocked(true);
    }

    @Override
    public void lockUserById(@NotNull String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final User user = userRepository.findById(id);
        if (user == null) return;
        user.setLocked(true);
    }

}
