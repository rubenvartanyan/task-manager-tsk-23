package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public @NotNull final E add(final @NotNull E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public void remove(final @Nullable E entity) {
        entities.remove(entity);
    }

    @Override
    public void removeById(@NotNull final String id) throws Exception {
        remove(entities.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null)
        );
    }

    @Override
    public final @Nullable E findById(@NotNull final String id) {
        return entities.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

}
