package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public final @NotNull List<Task> findAllByProjectId(@NotNull final String projectId,
                                                        @NotNull final String userId) {
        final @Nullable List<Task> listOfTask = new ArrayList<>();
        for (@Nullable final Task task: entities){
            if (task.getProjectId().equals(projectId) && task.getUserId().equals(userId)) listOfTask.add(task);
        }
        return listOfTask;
    }

    @Override
    public void removeAllByProjectId(final @NotNull String projectId,
                                     final @NotNull String userId) {
        for (final @Nullable Task task: entities) {
            if (task.getProjectId().equals(projectId) && task.getUserId().equals(userId)) task.setProjectId(null);
        }
    }

    @Override
    public final void bindTaskByProjectId(@NotNull final String projectId,
                                          @NotNull final String taskId,
                                          @NotNull final String userId) {
        final @NotNull Task task = Objects.requireNonNull(findById(taskId, userId));
        task.setProjectId(projectId);
    }

    @Override
    public final @NotNull Task unbindTaskFromProject(@NotNull final String projectId,
                                      @NotNull final String taskId,
                                      @NotNull final String userID) {
        final @NotNull Task task = Objects.requireNonNull(findById(taskId, userID));
        task.setProjectId(null);
        return task;
    }

}
