package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public final @NotNull List<E> findAll(final @NotNull String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public final @Nullable E findById(@NotNull final String id,
                      @NotNull final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public void clear(@NotNull final String userId) {
        entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .forEach(entities::remove);
    }

    @Override
    public void removeById(@NotNull final String id,
                           @NotNull final String userId) {
        entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst()
                .ifPresent(this::remove);
    }

    @Override
    public @Nullable final E findOneByIndex(@NotNull final Integer index,
                            @NotNull final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList())
                .get(index);
    }

    @Override
    public @Nullable final E findOneByName(@NotNull final String name,
                           @NotNull final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index,
                                 @NotNull final String userId) {
        remove(entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList())
                .get(index)
        );
    }

    @Override
    public void removeOneByName(@NotNull final String name,
                                @NotNull final String userId) {
        remove(entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null)
        );
    }

    @Override
    public final @NotNull List<E> findAll(final @NotNull Comparator<E> comparator,
                                          final @NotNull String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    public void showEntity(final @NotNull E entity) {
        System.out.println("[NAME: " + entity.getName() + " ]");
        System.out.println("[DESCRIPTION: " + entity.getDescription() + " ]");
        System.out.println("STATUS: " + entity.getStatus() + " ]");
    }

}
