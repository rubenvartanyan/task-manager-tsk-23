package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.ICommandRepository;
import ru.vartanyan.tm.command.AbstractCommand;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @Override
    public @NotNull Collection<AbstractCommand> getCommand() {
        return commands.values();
    }

    @Override
    public @NotNull Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public final @NotNull Collection<String> getCommandNames() {
        final @Nullable List<String> result = new ArrayList<>();
        for (final @Nullable AbstractCommand command: commands.values()) {
            final @Nullable String name = command.name();
            if (name == null || name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public @NotNull final Collection<String> getCommandsArgs() {
        final @Nullable List<String> result = new ArrayList<>();
        for (final @Nullable AbstractCommand command: commands.values()) {
            final @Nullable String argument = command.arg();
            if (argument == null || argument.isEmpty()) continue;
            result.add(argument);
        }
        return result;
    }

    @Override
    public final @NotNull AbstractCommand getCommandByName(final @NotNull String name) {
        return commands.get(name);
    }

    @Override
    public final @NotNull AbstractCommand getCommandByArg(final @NotNull String arg) {
        return commands.get(arg);
    }

    @Override
    public void add(final @NotNull AbstractCommand command) {
        final @Nullable String arg = command.arg();
        final @Nullable String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

}
