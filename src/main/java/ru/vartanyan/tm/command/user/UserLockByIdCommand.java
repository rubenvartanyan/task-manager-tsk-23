package ru.vartanyan.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractAuthCommand;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.util.TerminalUtil;

public class UserLockByIdCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-lock-by-id";
    }

    @Override
    public String description() {
        return "Lock user by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOCK USER BY ID]");
        System.out.println("[ENTER ID]");
        final @NotNull String id = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserById(id);
    }

    @Override
    public Role[] roles() {
        return new Role[] {
                Role.ADMIN
        };
    }

}
