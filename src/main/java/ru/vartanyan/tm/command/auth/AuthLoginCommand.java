package ru.vartanyan.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractAuthCommand;
import ru.vartanyan.tm.util.TerminalUtil;

public class AuthLoginCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String description() {
        return "Login";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGIN]");
        System.out.println("[ENTER LOGIN]");
        final @NotNull String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        final @NotNull String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

}
