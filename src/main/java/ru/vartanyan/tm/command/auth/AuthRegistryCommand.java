package ru.vartanyan.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractAuthCommand;
import ru.vartanyan.tm.util.TerminalUtil;

public class AuthRegistryCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String description() {
        return "Registry new user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REGISTRY]");
        System.out.println("[ENTER LOGIN]");
        final @NotNull String login = TerminalUtil.nextLine();
        System.out.println("[ENTER E-MAIL]");
        final @NotNull String email = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        final @NotNull String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
    }

}
