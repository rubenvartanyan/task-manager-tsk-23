package ru.vartanyan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public String description() {
        return "Update task by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER Id]");
        final @NotNull  String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findById(id, userId);
        System.out.println("[ENTER NAME]");
        final @NotNull String name = TerminalUtil.nextLine();
        System.out.println("[INTER DESCRIPTION]");
        final @NotNull String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().updateEntityById(id, name, description, userId);
        System.out.println("[TASK UPDATED]");
    }

}
