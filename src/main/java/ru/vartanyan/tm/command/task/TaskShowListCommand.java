package ru.vartanyan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.enumerated.Sort;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskShowListCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER SORT:]");
        System.out.println(Arrays.toString(Sort.values()));
        final @Nullable String sort = TerminalUtil.nextLine();
        List<Task> list;
        System.out.println(sort);
        if (sort == null || sort.isEmpty()) list = serviceLocator.getTaskService().findAll(userId);
        else {
            final @NotNull Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            list = serviceLocator.getTaskService().findAll(sortType.getComparator(), userId);
        }
        int index = 1;
        for (final @Nullable Task task: list) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
