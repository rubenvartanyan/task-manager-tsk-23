package ru.vartanyan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractTaskCommand{

    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove task by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE TASK]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER ID]");
        final @NotNull String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().removeById(id, userId);
        System.out.println("[TASK REMOVED]");
    }

}
