package ru.vartanyan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskShowByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-show-by-name";
    }

    @Override
    public String description() {
        return "Show task by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER NAME:]");
        final @NotNull String name = TerminalUtil.nextLine();
        final @NotNull Task task = serviceLocator.getTaskService().findOneByName(name, userId);
        serviceLocator.getTaskService().showEntity(task);
    }

}
