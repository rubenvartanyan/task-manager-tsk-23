package ru.vartanyan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.List;

public class TaskFindAllByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-find-all-by-project-id";
    }

    @Override
    public String description() {
        return "Find all tasks by project Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL TASKS BY PROJECT ID]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER PROJECT ID]");
        final @NotNull String projectId = TerminalUtil.nextLine();
        final @Nullable List<Task> tasks = serviceLocator.getProjectTaskService().findAllTaskByProjectId(projectId, userId);
        int index = 1;
        for (final @Nullable Task task: tasks){
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

}
