package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-start-by-index";
    }

    @Override
    public String description() {
        return "Start project by Index";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START PROJECT]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER INDEX]");
        final @NotNull Integer index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getProjectService().startEntityByIndex(index, userId);
    }

}
