package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectUpdateStatusByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-status-update-by-name";
    }

    @Override
    public String description() {
        return "Update project status by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT STATUS]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER NAME]");
        final @NotNull String name = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final @NotNull String statusId = TerminalUtil.nextLine();
        final @NotNull Status status = Status.valueOf(statusId);
        serviceLocator.getProjectService().updateEntityStatusByName(name, status, userId);
        System.out.println("[PROJECT STATUS UPDATED]");
    }

}
