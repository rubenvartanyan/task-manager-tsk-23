package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Override
    public String description() {
        return "Update project by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER Id]");
        final @NotNull String id = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(id, userId);
        System.out.println("[ENTER NAME]");
        final @NotNull String name = TerminalUtil.nextLine();
        System.out.println("[INTER DESCRIPTION]");
        final @NotNull String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().updateEntityById(id, name, description, userId);
        System.out.println("[PROJECT UPDATED]");
    }

}
