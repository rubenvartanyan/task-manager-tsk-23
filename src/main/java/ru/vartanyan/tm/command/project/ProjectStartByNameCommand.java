package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-start-by-name";
    }

    @Override
    public String description() {
        return "Start project by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START PROJECT]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER NAME]");
        final @NotNull String name = TerminalUtil.nextLine();
        serviceLocator.getProjectService().startEntityByName(name, userId);
        System.out.println("[PROJECT STARTED]");
    }

}
