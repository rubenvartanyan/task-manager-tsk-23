package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectAndTaskRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-and-tasks-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove project and all its tasks by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE ALL TASKS FROM PROJECT AND THEN PROJECT]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER PROJECT ID]");
        final @NotNull String projectId = TerminalUtil.nextLine();
        serviceLocator.getProjectTaskService().removeProjectById(projectId, userId);
        System.out.println("TASKS REMOVED FROM PROJECT");
        System.out.println("PROJECT REMOVED");
    }

}
