package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectUpdateStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-status-update-by-id";
    }

    @Override
    public String description() {
        return "Update project status by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT STATUS]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER ID]");
        final @NotNull String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final @NotNull String statusId = TerminalUtil.nextLine();
        final @NotNull Status status = Status.valueOf(statusId);
        serviceLocator.getProjectService().updateEntityStatusById(id, status, userId);
        System.out.println("[PROJECT STATUS UPDATED]");
    }

}
