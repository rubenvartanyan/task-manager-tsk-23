package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.enumerated.Sort;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER SORT:]");
        System.out.println(Arrays.toString(Sort.values()));
        final @Nullable String sort = TerminalUtil.nextLine();
        List<Project> list;
        if (sort == null || sort.isEmpty()) list = serviceLocator.getProjectService().findAll(userId);
        else {
            final @NotNull Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            list = serviceLocator.getProjectService().findAll(sortType.getComparator(), userId);
        }
        int index = 1;
        for (final @Nullable Project project: list) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

}
